package com.mastertech.itau.kafka.example.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.core.KafkaTemplate;


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.mastertech.itau.kafka.example.*")
public class KafkaExampleApplication implements CommandLineRunner {
    public static Logger logger = (Logger) LoggerFactory.getLogger(KafkaExampleApplication.class);

    @Value("${com.mastertech.itau.kafka.example.kafka.topic.game}")
    private String gameTopic;

    @Autowired
    private KafkaTemplate<String, String> template;

    public static void main(String[] args) {
        SpringApplication.run(KafkaExampleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        for (int i = 0; i < 1000; i++) {
            System.out.println(i);
            template.send(gameTopic, "c2.log.michel" + Integer.toString(i), "test message - " + i);
            logger.info("Mensagem enviada: " + "test message - " + i);
        }
    }
}
