package com.mastertech.itau.kafka.example.application.queue;

import com.mastertech.itau.kafka.example.application.KafkaExampleApplication;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class ListenerExample {
    public static Logger logger = (Logger) LoggerFactory.getLogger(KafkaExampleApplication.class);

    @Value("${com.mastertech.itau.kafka.example.kafka.topic.game}")
    private String gameTopic;

    @KafkaListener(topics = "${com.mastertech.itau.kafka.example.kafka.topic.game}")
    public void receive(ConsumerRecord record) {
        logger.info(String.format(">>>>>>>>>>>>>>>Topic - %s, Partition - %d, Value: %s", gameTopic, record.partition(), record.value()));
    }
}
